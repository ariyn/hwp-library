## Hwp Library

이 프로젝트는 부대내 행정계 병사들이 하루에도 수십-수백개의 한글파일을 열고, 복사하고, 다시 붙여넣는 작업이 너무나 안타까워 개발하게 되었습니다.
물론 사회에서는 여러 라이브러리를 사용할 수 있었게지만, 부대내 사무용 PC의 한계덕분에 .hwp파일 파서부터 만들어야 했습니다.

pytest, travis, git... 등등 개발지원툴이 전무한 상황에서 개발된 프로젝트인지라 아직 소수의 unittest를 제외한다면 순수 파이선 소스만 레포지토리에 업로드되어 있습니다.

현재 Master브랜치는 몇몇 오타/명확한 파이썬 버그를 제외한다면 부대내 PC로 개발된 버전과 동일합니다. 이후 업데이트가 진행될때, Millitary 버전으로 태깅될것입니다.

## 사용법
```python
from hwp import HWP

hwpFile = HWP("path/to/file.hwp")
print(hwpFile.body.children[0].text)

strings = [c.text for c in hwpFile.body.children]
with open("sample","w") as f:
	f.write("\n".join(strings))
```

## Things to do
* [ ] repackaging
* [ ] more obvious api
* [ ] improve hwp parsing algorithm
