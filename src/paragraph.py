import io

from . import HWPID
from .tools import endian, MAKE4CHID, endstr, endian, endint
from .hex import dump

class stream:
	pass

def adder(tagId):
	return lambda self, value:self.categorizedComponents[tagId].append(value)

def parameter(f):
	def _(self, _section, *args, **kwargs):
		return f(self, _section, *args, param=True, **kwargs)
	return _

def section(f):
	def _(self, _section, *args, **kwargs):
		return f(self, _section, section=True, **kwargs)
	return _

def attributes(name = "", size=0, dataType=int, version=HWPID.LATEST_VERSION, section=False, param=False, autoRead=True):
	def _(f):
		def __(self, _section, *args, dataSet=[], doRepeat=False, **kwargs):
			parent = f.__qualname__.split(".")[0]
			if not doRepeat:
				dataSet = []
				doRepeat = True
				
			if name:
				dataSet.append(tuple([name, size, dataType, version]))

			if parent != "attributes":
				__collectedSize__ = 0
				reqSize = 0

				if not hasattr(self, "attributeNames"):
					self.attributeNames = []
				for _name, _size, _dataType, _version in dataSet:
					sf = _section.readInt if _dataType==int else _section.read
					typeName = type(_size).__name__

					if typeName == "lambda" or typeName == "function":
						_size = _size(self)
					value = sf(_size)

					if not hasattr(self, "size"):
						self.size = 0
					self.size += _size
					__collectedSize__ += _size

					if not param and "param" not in kwargs:
						self.__setattr__(_name, value)
						self.attributeNames.append(_name)
					else:
						kwargs[_name] = value

				if "param" in kwargs:
					del kwargs["param"]

				if "section" in kwargs:
					isSection = kwargs["section"]
					del kwargs["section"]
				else:
					isSection = False

				if "size" in kwargs:
					if autoRead:
						_section.read(kwargs["size"]-__collectedSize__)
					reqSize = kwargs["size"]
					del kwargs["size"]

				if section or isSection:
					return f(self, section=_section, size=reqSize, **kwargs)
				else:
					return f(self, **kwargs)
			else:
				return f(self, _section, dataSet=dataSet, doRepeat=doRepeat, **kwargs)
		return __
	return _

attrs = attributes

class Components:
	__close__ = False
	@staticmethod
	def getName(tagId):
		name = HWPID.HWPTAG_ID_VALUE_MAPPING[tagId].lower().replace("hwptag_", "")
		name = "add"+"".join([i[0].upper()+i[1:] for i in name.split("_")])
		return name
	
	def __init__(self, category=None, childFunc=lambda s:None, childList=None):
		self.components = []
		self.categorizedComponents = {}
		self.category = category if category else []
		self.children = []
		childList = [] if not childList else childList
		if not hasattr(self, "attributeNames"):
			self.attributeNames = []
		
		for k in self.category:
			self.categorizedComponents[k] = []
		
		for i in childList:
			name = Components.getName(i)
			def _(self, name):
				def __(sect, size):
					child = childFunc(self)
					func =  child.__getattribute__(name)
					return func(sect, size=size)
				return __
			self.__setattr__(name, _(self, name))
	
	def add(self, tagId, section, size):
		retVal = None
		if tagId in self.categorizedComponents:
			name = Components.getName(tagId)
			retVal = self.__getattribute__(name)(section, size=size)
		return retVal
	
	def repackage(self):
		pass
	
	def __str__(self):
		return self.__repr__()
	def __repr(self):
		return "<%s>"%self.__name__
	
class Root(Components):
	__name__ = "ROOT"
	def __init__(self, level=0):
		self.size = 0
		self.level = level
		super(Root, self).__init__([HWPID.HWPTAG_PARA_HEADER])

	def addParaHeader(self, section, size):
		p = Paragraph(section, size=size)
		self.children.append(p)
		return p

	def repackage(self):
		x = b""
		for i in self.children:
			x += i.repackage()
		return x
		
class Text(Components):
	__name__ = "TEXT"
	__close__ = "True"
	__defaultValues__ = {"text":""}
	
	@attrs("bText", lambda s:s.nChars*2, stream)
	def __init__(self):
		self.text = self.bText.decode("utf-16")
		
class Paragraph(Components):
	__name__ = "PARAGRAPH"
	__close__ = True
	__defaultValues__ = {
		"controlMask":0,
		"shapeId":0,
		"styleId":0,
		"column":0,
		"columnStart":0,
		"charShapeLen":1,
		"rangeTagLen":0,
		"instanceId":0,
		"isMerged":0,
		"startLocation":0,
		"lineHeight":1000,
		"textHeight":1000,
		"baseToVertical":850,
		"lineSeparation":600,
		"segmentWidth":42520,
		"tag":393216
	}
	
	@attrs("nChars", 4)
	@attrs("controlMask", 4)
	@attrs("shapeId", 2)
	@attrs("styleId", 1)
	@attrs("column", 1)
	@attrs("charShapeLen", 2)
	@attrs("rangeTagLen", 2)
	@attrs("alignLen", 2)
	@attrs("instanceId", 4)
	@attrs("isMerged", 2)
	def __init__(self):
		c = [HWPID.HWPTAG_PARA_TEXT, HWPID.HWPTAG_PARA_CHAR_SHAPE, HWPID.HWPTAG_PARA_LINE_SEG, HWPID.HWPTAG_PARA_RANGE_TAG, HWPID.HWPTAG_CTRL_HEADER]
		self.text = ""
		self.nCharsAnd = False
		
		if self.nChars&0x80000000:
			self.nChars &= 0x7fffffff
			self.nCharsAnd = True
		super(Paragraph, self).__init__(
			c,
			lambda s:s.nodes[-1],
			[HWPID.HWPTAG_SHAPE_COMPONENT, HWPID.HWPTAG_SHAPE_COMPONENT_RECTANGLE, HWPID.HWPTAG_SHAPE_COMPONENT_ELLIPSE]
		)	
		self.charShapes = []
		self.nodes = []
	
	@attrs("bText", lambda s:s.nChars*2, stream)
	def addParaText(self):
		self.text = self.bText.decode("utf-16")
	
	@attrs("charShape", lambda s:s.charShapeLen*8, stream, param=True)
	def addParaCharShape(self, charShape):
		for i in range(self.charShapeLen):
			s = endint(charShape[i:i+4])
			e = endint(charShape[i+4:i+8])
			self.charShapes.append((s,e))
	
	@attrs("startLocation", 4)
	@attrs("verticalLocation", 4)
	@attrs("lineHeight", 4)
	@attrs("textHeight", 4)
	@attrs("baseToVertical", 4)
	@attrs("lineSeparation", 4)
	@attrs("columnStart", 4)
	@attrs("segmentWidth", 4)
	@attrs("tag", 4)
	def addParaLineSeg(self):
		t = self.tag
		self.isFirstLineOfPage = t&1
		self.isFirstLineOfColumn = (t>>1)&1
		self.isEmptySegment = (t>>15)&1
		self.isFirstSegment = (t>>16)&1
		self.isLastSegment =  (t>>17)&1
		self.isAutoHyphenation = (t>>18)&1
		self.isIndentation = (t>>19)&1
		self.isHeadShape = (t>>20)&1
	
# 	@attrs("__range__", lambda s:s.rangeTagLen*12, stream)
# 	def addParaRangeTag(self):
# 		pass
		
	@parameter
	@attrs("ctrlHeader", 4, stream)
	@attrs(section = True, autoRead=False)
	def addCtrlHeader(self, section, size, ctrlHeader=None):
		ctrlHeader = MAKE4CHID(ctrlHeader)
		node = None
		
		if ctrlHeader == "secd":
			node = SectionDefinition(section, size=size-4)
		elif ctrlHeader == "cold":
			node = ColumnDefinition(section, size=size-4)
		elif ctrlHeader == "tbl":
			node = Table(section)
		elif ctrlHeader == "gso":
			node = GenShapeObject(section, size=size-4)
		
		self.nodes.append(node)
		return node
	
	
class SectionDefinition(Components):
	__close__ = True
	__name__ = "SectionDefinition"
	
	@attrs("attribute", 4)
	@attrs("columnMargin", 2)
	@attrs("verticalLineSet", 2)
	@attrs("horizontalLineSet", 2)
	@attrs("defaultTabSize", 4)
	@attrs("paragraphShapeId", 2)
	@attrs("pageNumber", 2)
	@attrs("pictureNumber", 2)
	@attrs("tableNumber", 2)
	@attrs("equationNumber", 2)
	@attrs("defaultLanguage", 2, version="5.0.1.5")
	def __init__(self):
		c = [HWPID.HWPTAG_PAGE_DEF, HWPID.HWPTAG_FOOTNOTE_SHAPE, HWPID.HWPTAG_PAGE_BORDER_FILL]
		super(SectionDefinition, self).__init__(category = c)
		self.ctrlHeader = "secd"
	
	@parameter
	@attrs("pageWidth", 4)
	@attrs("pageHeight", 4)
	@attrs("leftMargin", 4)
	@attrs("rightMargin", 4)
	@attrs("topMargin", 4)
	@attrs("bottomMargin", 4)
	@attrs("hNoteMArgin", 4)
	@attrs("tNoteMargin", 4)
	@attrs("bindingMargin", 4)
	@attrs("pageDefAttr", 4)
	def addPageDef(self, **kwargs):
		pass
	
	