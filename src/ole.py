from .tools import endian, endint, endstr

class WrongOleFileException(Exception):
	pass

class DirectoryEntry:
	_types = ["Empty", "UserStorage", "UserStream", "LockBytes", "Property", "RootStorage"]
	_colors = ["Red", "Black"]
	def __init__(self, target, dirId):
		self.offset = dirId*128
		self.sector = []
		
		_dif = {
			"_ab"						:self.read(64, target),
			"_cb"						:self.read(OLE.WORD, target),
			"_mse"					:self.read(OLE.BYTE, target),
			"_bflags"				:self.read(OLE.BYTE, target),
			"_sidLeftSib"		:self.read(OLE.SID, target),
			"_sidRightSib"	:self.read(OLE.SID, target),
			"_sidChild"			:self.read(OLE.SID, target),
			"_clsId"				:self.read(OLE.GUID, target),
			"_dwUserFlags"	:self.read(OLE.DWORD, target),
			"_time"					:self.read(OLE.TIME_T, target)+self.read(OLE.TIME_T, target),
			"_sectStart"		:self.read(OLE.SECT, target),
			"_ulSize"				:self.read(OLE.ULONG, target),
			"_dptPropType"	:self.read(OLE.DFPROPTYPE, target)
		}
		
		try:
			self.name = _dif["_ab"][:endint(_dif["_cb"])].decode("utf-16")[:-1]
		except:
			print(dirId, _dif["_ab"], target, )
		self.type = self._types[endint(_dif["_mse"])]
		self.color = self._colors[endint(_dif["_bflags"])]
		
		self.sectorStart = endint(_dif["_sectStart"])
		self.sectorSize = endint(_dif["_ulSize"])
		self.sectorType = "SSAT" if 0 < self.sectorSize < 4096 else "SAT"
		self.data = self.read(self.sectorSize, target)
		
		_dif["size"] = sum([len(v.hex())//2 for i,v in _dif.items()])
		self.offset = 0
		self._dirEntry = _dif
		
		##?? self.sector = target
		
	def read(self, size, target=None):
		if not target:
			target = self.sector
		self.offset += size
		return bytes(target[self.offset-size:self.offset])

	def readInt(self, size, target=None):
		return int(endian(self.read(size).hex()), 16)

	def write(self, data):
		raise NotImplemented
	def writeInt(self, int):
		self.write(endstr(int))

	def seek(self, offset):
		self.offset += offset
	def __getitem__(self, key):
		if key in self._dirEntry:
			return self._dirEntry[key]
		else:
			return None

class OLE:
	ULONG						 = 4
	USHORT					 = 2
	OFFSET					 = 2
	SECT						 = 4
	FSINDEX					 = 4
	FSOFFSET				 = 2
	DFSIGNATURE			 = 4
	BYTE 						 = 1
	WORD 						 = 2
	DWORD 					 = 4
	DFPROPTYPE			 = 4
	SID							 = 4
	CLSID = GUID		 = 16
	TIME_T = FILETIME = 8
	
	DIFSECT 				 = 0XFFFFFFFC
	FATSECT					 = 0XFFFFFFFD
	ENDOFCHAIN			 = 0XFFFFFFFE
	FREESECT				 = 0XFFFFFFFF
	SPECIAL_SECTS = [DIFSECT, FATSECT, ENDOFCHAIN, FREESECT]
	
	sect2byte = lambda self,sect:(sect<<int(endian(self.header["_uSectorShift"].hex()), 16))+512
	short2byte = lambda self, sect:(sect<<int(endian(self.header["_uMiniSectorShift"].hex()), 16))
	
	def __init__(self, target):
		self.file = target if hasattr(target, "read") else open(target, "rb")
		self.bData = self.file.read()
		self.offset, self.index = 0,0
		self.bitOffset = 0
		
		self.header = {}
		self.dirEntry, self._dirEntry = [], {}
		self.parse()
		
	def keys(self):
		return self._dirEntry.keys()
	def __getitem__(self, key):
		if key in self._dirEntry:
			return self._dirEntry[key]
		else:
			return None
	
	def parse(self):
		self.header = {
			"_abSig":self.read(self.BYTE*8),
			"_clid":self.read(self.CLSID),
			"_uMinorVersion":self.read(self.USHORT),
			"_uDllVersion":self.read(self.USHORT),
			"_uByteOrder":self.read(self.USHORT),
			"_uSectorShift":self.read(self.USHORT),
			"_uMiniSectorShift":self.read(self.USHORT),
			"_usReserved":self.read(self.USHORT),
			"_ulReserved1":self.read(self.ULONG),
			"_ulReserved2":self.read(self.ULONG),
			"_casectFat":self.read(self.FSINDEX),
			"_sectDirStart":self.read(self.SECT),
			"_signiture":self.read(self.DFSIGNATURE),
			"_ulMiniSectorCutoff":self.read(self.ULONG),
			"_sectMiniFatStart":self.read(self.SECT),
			"_casectMiniFat":self.read(self.FSINDEX),
			"_sectDifStart":self.read(self.SECT),
			"_casectDif":self.read(self.FSINDEX),
			"_sectFat":[self.read(self.SECT) for i in range(0,109)],
		}
		if self.header["_abSig"].hex() != "d0cf11e0a1b11ae1":
			raise WrongOleFileException
		self.header["size"] = sum([len(v.hex())//2 if type(v) == bytes else len(v) for i,v in self.header.items()])
		self.seek(0, True)
		self.header["raw"] = self.read(self.header["size"])
		
		self.header["sat sector length"] = endint(self.header["_casectFat"])
		self.header["first directory"] = endint(self.header["_sectDirStart"])
		self.header["sector size"] = 1<<endint(self.header["_uSectorShift"])
		
		self.header["short sector size"] = 1<<endint(self.header["_uMiniSectorShift"])
		self.header["short sector start"] = endint(self.header["_sectMiniFatStart"])
		self.header["short sector number"] = endint(self.header["_casectMiniFat"])
		
		self.buildMSAT()
		self.buildSAT()
		self.buildSSAT()
		self.buildDirectoryEntry()
		
	def compress(self):
		raise NotImplemented
	
	def parseDIF(self, target, difId=0):
		raise NotImplemented
	
	def readSector(self, secId):
		raise NotImplemented
	
	def read(self, byte=-1, target=None):
		if not target:
			target = self.bData
		self.offset += byte
		return bytes(target[self.offset-byte:self.offset])
	
	def seek(self, index, ab=False):
		o = self.offset
		if ab:
			self.offset = index
		else:
			self.offset += index
		return o
	
	def buildMSAT(self):
		mastStart = endian(self.header["_sectDifStart"].hex())
		mastSize = endint(self.header["_casectDif"])
		mast = [endian(self.header["_sectFat"][i].hex()) for i in range(109)]
		
		nextSec, index = mastStart, 0
		
		while nextSec not in self.SPECIAL_SECTS and index<mastSize:
			o = self.seek(nextSec)
			mast += [endian(self.read(self.SECT).hex()) for i in range(self.header["sector size"])]
			self.seek(o)
			nextSec = mast[-1]
			index += 1
		
# 		self.seek(0x200, abs=True)
		self.MAST = mast
		return mast
			
	def buildSAT(self):
		sats = []
		for i in self.MAST[:self.header["sat sector length"]]:
			index = self.sect2byte(int(i,16))
			o = self.seek(index, True)
			sats += [endian(self.read(self.SECT).hex()) for j in range(self.header["sector size"]//4)]
			self.seek(o, True)
		self.SAT = sats
		return sats
			
	def buildSSAT(self):
		d = []
		ids, body = self.getSecLists(self.header["short sector start"])
		body = [[j for j in body[i:i+4][::-1]] for i in range(0, len(body), 4)]
		body = [hex((b[0]<<24)+(b[1]<<16)+(b[2]<<8)+b[3])[2:] for b in body]
		self.SSAT = body
		return body
	
	def buildDirectoryEntry(self):
		ids, body = self.getSecLists(self.header["_sectDirStart"])
		parseList, doneList = [0], []
		difList, difDict = [], {}
		while parseList:
			i = parseList.pop(0)
			dif = DirectoryEntry(body, i)
			
			for name in ["_sidLeftSib", "_sidRightSib", "_sidChild"]:
				num = endint(dif[name])
				if num not in self.SPECIAL_SECTS:
					parseList.append(num)
			
			if dif.sectorType == "SAT":
				ids, sscs = self.getSecLists(dif.sectorStart)
				dif.sector = bytes(sscs)
				dif.sectorIds = ids
				
				if dif.name == "Root Entry":
					self.SSCS = sscs

			elif dif.sectorType == "SSAT":
				if dif.name == "Root Entry":
					dif.sectorIds, sscs = self.getSecLists(dif.sectorStart)
					self.SSCS = sscs
				else:
					dif.sectorIds, sscs = self.getShortSecLists(dif.sectorStart, dif.sectorSize)
				dif.sector = bytes(sscs)
			doneList.append(i)
			difList.append(dif)
			difDict[dif.name] = dif
		self.dirEntry = difList
		self._dirEntry = difDict
		
	def getShortSecLists(self, sectId, size):
		target = self.SSCS
		if type(sectId) == bytes:
			sectId = sectId.hex()
		elif type(sectId) == int:
			sectId = "%02x"%sectId
		
		nextId = int(sectId, 16)
		sectList, sectBody = [], []
		
		while nextId not in self.SPECIAL_SECTS:
			sectList.append(nextId)
			index = self.short2byte(nextId)
			sectBody += target[index:index+size]
			nextId = int(self.SSAT[nextId], 16)
		return sectList, sectBody
		
	def getSecLists(self, secId):
		if type(secId) == bytes:
			secId = secId.hex()
			nextId = endian(secId)
		elif type(secId) == int:
			secId = "%02x"%secId
			nextId = secId
		
		sectList, sectBody = [], []
		nextId = int(nextId, 16)
		while nextId not in self.SPECIAL_SECTS:
			sectList.append(nextId)
			index = self.sect2byte(nextId)
			sectBody += self.bData[index:index+self.header["sector size"]]
			nextId = int(self.SAT[nextId], 16)
		return sectList, sectBody