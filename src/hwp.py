import re
import zlib
import os

from .tools import endian, endint, MAKE4CHID
from .ole import *
from .paragraph import *
from . import HWPID

class WRONG_HWP_FILE_EXCEPTION:
	pass

def Record(originalTagId):
	def _(f):
		def __(*args, tagId=None, level=None, size=None, **kwargs):
			if not (tagId or level or size):
				try:
					section = [i for i in args if type(i) == DirectoryEntry][0]
					header = section.readInt(4)
					tagId = header&0x3ff
					level = (header>>10)&0x3FF
					size = (header>>20)
				except IndexError:
					raise WRONG_HWP_FILE_EXCEPTION
				
			if tagId != nriginalTagId:
				raise WRONG_HWP_FILE_EXCEPTION
			if size == 4096:
				size = section.readInt(4)
			return f(*args, tagId=tagId, level=level, size=size, **kwargs)
		return __
	return _

def Args(*argName):
	def _(f):
		def __(*args, **kwargs):
			kwargs = {key:value for key,value in kwargs.items() if key in argName}
			return f(*args, **kwargs)
		return __
	return _

class HWP(OLE):
	FILE_ATTRIBUTES = [
		"isCompressed", "password", "published", "script", "DRM", "XML storage",
		"history", "signature", "certificate", "certificatedDRM", "CCL"
	]
	
	### sectionData = section.readInt(4)
	@staticmethod
	def parseHeader(section):
		header = section.readInt(4)
		return header&0x3FF, (header>>10)&0x3FF, (header>>20)
	
	def __init__(self, fileName):
		super(HWP, self).__init__(fileName)
		
		self.parseHWP()
		
	def readBit(self, bit=1):
		pass
	
	def parseHWP(self):
		matches = re.compile(r"<[a-zA-Z0-9_]+>")
		
		fh = self["FileHeader"]
		fh = {
			"signature":fh.read(32),
			"_ver":endian(fh.read(4).hex()),
			"_attr":fh.readInt(4),
			"attributes":{},
			"reserved":fh.read(216)
		}
		
		fh["version"] = ".".join(["%d"%int(fh["_ver"][i:i+2], 16) for i in range(0, len(fh["_ver"]), 2)])
		
		for i, v in enumerate(self.FILE_ATTRIBUTES):
			fh["attributes"][v] = fh["_attr"]&(2**i)
		
		self.isCompressed = fh["attributes"]["isCompressed"]
		self.version = fh["version"]
		
		self.docInfo = self.parseDocInfo(self["DocInfo"])
		self.body = self.parseBodyText(self["Section0"])
		
	def parseDocInfo(self, di):
		if self.isCompressed:
			di.sector = zlib.decompress(di.sector, -15)
		tagId, level, size = HWP.parseHeader(di)
		
		documentProperty = {
			"areaCount":di.readInt(2),
			"pageStartNum":di.readInt(2),
			"footnoteStartNum":di.readInt(2),
			"endNoteStartNum":di.readInt(2),
			"pictureStartNum":di.readInt(2),
			"tableStartNum":di.readInt(2),
			"formulaStartNum":di.readInt(2),
			"listId":di.readInt(4),
			"paragraphId":di.readInt(4),
			"characterPosition":di.readInt(4)
		}
		mapping = self.parseDocMapping(self["DocInfo"])

		for i in range(mapping[0]):
			self.parseBinaryData(self["DocInfo"])
		for i in range(sum(mapping[1:8])):
			self.parseFontName(self["DocInfo"])
		for i in range(mapping[8]):
			self.parseBorderFill(self["DocInfo"])
		for i in range(mapping[9]):
			self.parseCharacterShape(self["DocInfo"])
		for i in range(mapping[10]):
			self.parseTab(self["DocInfo"])
		return documentProperty

	def parseDocMapping(self, di):
		###########################################
		## reuse this parsing header code
		## decorator? function?
		tagId, level, size = HWP.parseHeader(di)
		############################################
		
		idMapping = [di.readInt(4) for i in range(18)]
		return idMapping
	
	def parseBinaryData(self, di):
		tagId, level, size = HWP.parseHeader(di)
		if tagId != HWPID.HWPTAG_BIN_DATA:
			raise WRONG_HWP_FILE_EXCEPTION
		
		attr = di.readInt(2)
		attrType, attrCompress = attr&0x15, (attr>>4) & 0x3
		
		if attrType == 0:
			### shouldn't path, path2 be endian???? but readInt??
			length = di.readInt(2)
			path = di.readInt(length*2)
			length2 = di.readInt(2)
			path2 = di.readInt(length2*2)
		elif attrType == 1:
			uid = di.readInt(2)
			length = di.readInt(2)
			name = di.read(length*2).decode("utf-16")
		elif attrType == 2:
			uid = di.readInt(2)
	
	def parseFontName(self, di):
		tagId, level, size = HWP.parseHeader(di)
		if tagId != HWPID.HWPTAG_FACE_NAME:
			raise WRONG_HWP_FILE_EXCEPTION
		
		attr = di.readInt(1)
		fontData = (attr&0x80!=0, attr&0x40!=0,attr&0x20!=0)
		nameLength = di.readInt(2)
		name = "".join([chr(di.readInt(2)) for i in range(nameLength)])
		
		font = {
			"attr":fontData,
			"name":name,
		}
		
		if fontData[0]:
			altType = di.readInt(1)
			altNameLength = di.readInt(2)
			altName = "".join([chr(di.readInt(2)) for i in range(altNameLength)])
			font["altName"] = altName
		
		if fontData[1]:
			font["fontType"] = di.read(10)
		
		if fontData[2]:
			defaultNameLength = di.readInt(2)
			defaultName = "".join([chr(di.readInt(2)) for i in range(defaultNameLength)])
			font["defaultName"] = defaultName
			
		return font
	
	def parseBorderFill(self, di):
		pass
	
	def parseCharacterShape(self, di):
		pass
	
	def parseTab(self, di):
		pass
	
	def parseNumbering(self, di):
		pass
	
	def parseBodyText(self, section):
		if self.isCompressed:
			section.sector = zlib.decompress(section.sector, -15)
		header = True
		currentNodes = [Root()]
		lastNode, lastLevel = None, -1
		
		while header and section.offset < section.sectorSize:
			node = None
			tagId, level, size = HWP.parseHeader(section)
			
			if level <= lastLevel:
				while currentNodes and level <= currentNodes[-1].level and currentNodes[-1].__close__:
					currentNodes.pop(-1)
			paragraph = currentNodes[-1]
			node = paragraph.add(tagId, section, size)
			if node:
				currentNodes.append(node)
				node.level = level
			lastLevel = level
		return currentNodes[0]

class BorderBackground:
	def __init__(self, attr):
		self.has3D = attr&1
		self.hasShadow = (attr>1)&1