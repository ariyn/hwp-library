
endian = lambda hexValue:"".join([hexValue[i]+hexValue[i+1] for i in range(0, len(hexValue), 2)][::-1])
endint = lambda target:int(endian(target.hex()), 16)
endstr = lambda target, max=4:bytes.fromhex(endian(("%0%dx".replace("%d", str(max*2)))%target))
MAKE4CHID = lambda bytes:bytes.decode("utf-8")[::-1]
UNMAKE4CHID = lambda str:str.encode("utf-8")[::-1]