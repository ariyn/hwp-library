def dump(bytes, offset=0, size=None):
	string = ""
	size = len(bytes) if not size else size
	
	for i in range(0, size, 16):
		string += "%06X:\t\t%s\t\t%s\t\t%s\n"%(
			i+offset,
			" ".join(["%02X"%e for e in bytes[i:min(i+8,size)]]),
			" ".join(["%02X"%e for e in bytes[i+8:min(i+16,size)]]),
			""
		)
	print(string)
