from math import ceil
replaceTable = {
	"_s":"shape",
	"_c":"component",
	"_p":"para"
}
hwptagPrefix = "HWPTAG_"
idNames = "id mappings,  bin data,  face name,  border fill,  char shape ,  tab def ,  numbering ,  bullet ,  numbering ,  _p header ,  _p text ,  _p char shape ,  _p line seg ,  _p range tag ,  ctrl header ,  list header ,  page def ,  footnote _s ,  page border fill ,  _s _c ,  table , _s _c line, _s _c rectangle, _s _c ellipse, _s _c arc, _s _c polygon, _s _c curve, _s _c ole, _s _c picture, _s _c container, ctrl data, _s _c eqedit, _s _c textart, form object, memo shape, memo list, chart data, video data, _s _c unknown"
startValue = ("HWPTAG_BEGIN", 0x10)
startFormat = "%s%s= %d"%(startValue[0], "%s", startValue[1])
difValue = [1,2,3,4,5,6,7,8,9,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,74,75,76,77,79,82,99]

reserved = {"RESERVED":73}
defaultFormat = "%s%s= HWPTAG_BEGIN+%d"
mappingFormat = ("\t\"%s\":%s,", "\t%s:\"%s\",")

idNames = [i.strip() for i in idNames.split(",")]

for i, v in enumerate(idNames):
	tokens = v.split(" ")
	for t in tokens:
		if t in replaceTable:
			v = v.replace(t, replaceTable[t])
	idNames[i] = (hwptagPrefix+v.replace(" ", "_").upper(), difValue[i])

for i, v in reserved.items():
	idNames.append((i, v))

maxLen = ceil(max([len(i) for i,v in idNames])/10)*10
idNames = sorted(idNames, key=lambda a:a[1])

variableDeclaration = "\n".join([startFormat%" "*(maxLen-len(name))]+[defaultFormat%(name, " "*(maxLen-len(name)), value) for name, value in idNames])

mappingDeclaration = "HWPTAG_ID_VALUE_MAPPING = {\n"+ \
	"\n".join([mappingFormat[0].replace("%s", name) for name,_ in idNames])+ \
	"\n\n"+ \
	"\n".join([mappingFormat[1].replace("%s", name) for name,_ in idNames])[:-1]+ \
	"\n}"

open("./HWPID.py", "w").write(variableDeclaration+"\n\n"+mappingDeclaration+"\n\nLATEST_VERSION = \"5.0.5.0\"")
