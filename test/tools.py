import os

def collectSampleFiles(sampleFiles, g=None):
	baseDir = os.path.dirname(__file__)
	sampleDatas = {}
	if not g:
		g = globals()
	for name, path in sampleFiles:
		with open(os.path.join(baseDir,path), "r") as f:
			g[name] = f.read()