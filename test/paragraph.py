import unittest
from io import BytesIO

import src.paragraph as p
from src.hex import dump
import src.HWPID as HWPID
	
testTextSource = "this is sample text"

class ParagraphUnitTest(unittest.TestCase):
	def test_AttributeSample(self):
		def _(self):
			self.testText = self.testText.decode("utf-8")
			return self.testText
		setattr(_, "testTextSource", testTextSource)
		setattr(_, "nChars", len(_.testTextSource))
		testSection = BytesIO(_.testTextSource.encode("utf-8"))

		x = p.attributes("testText", lambda s:s.nChars*2, p.stream)
		d = x(_)
		result = d(_, testSection)
		
		self.assertEqual(_.testTextSource, result)
		self.assertEqual(_.testTextSource, _.testText)
		
	def test_getComponent(self):
		root = p.Root()
		func = p.Components.getName(HWPID.HWPTAG_PARA_HEADER)
		self.assertEqual(root.addParaHeader, func)