import unittest
import os
import itertools
import io

from copy import copy, deepcopy

from .tools import collectSampleFiles
from src.ole import DirectoryEntry, OLE
from src.hex import dump
from src.tools import endint


### sample from https://www.openoffice.org/sc/compdocfileformat.pdf
### chapter 8.5.1
sampleFiles = [
	("sampleOldHeader", "sampleFiles/header"),
	("sampleDirectory", "sampleFiles/directory1"),
	("sampleDirectory2", "sampleFiles/directory2"),
	("sampleSat", "sampleFiles/sat"),
	("sampleSsat", "sampleFiles/ssat")
]
hwpDirectoryList = [
	"\5HwpSummaryInformation",
	"BodyText",
	"PrvImage",
	"Root Entry",
	"Section0",
	"Section1",
	"Section2",
	"Scripts",
	"_LinkDoc",
	"DefaultJScript",
	"FileHeader",
	"DocOptions",
	"DocInfo",
	"JScriptVersion",
	"PrvText"
]

bytesHex = lambda intList:bytes(intList).hex().upper()
def parseSample(sampleDirectory):
	dirString = sampleDirectory.split("\n")
	dirString = [[int(e, 16) for e in i[10:].split(" ")] for i in dirString]
	return list(itertools.chain.from_iterable(dirString))

def emptySample(size):
	string = []
	base = "000000000 "
	for i in range(0, size, 16):
		string.append(base+" ".join(["00" for e in range(i, min(i+16, size))]))	
	return parseSample("\n".join(string))

def copyOLE(*_escapeArguments):
	escapeArguments = []
	for i in _escapeArguments:
		if type(i) == str:
			escapeArguments.append((i, lambda x:None))
		elif type(i) == tuple:
			escapeArguments.append(i)
	
	def _(f):
		def __(*args, **kwargs):
			# https://stackoverflow.com/questions/9541025/how-to-copy-a-python-class
			_OLE = type('_OLE', OLE.__bases__, dict(OLE.__dict__))
			for i in escapeArguments:
				setattr(_OLE, i[0], i[1])
			return f(*args, OLE=_OLE, **kwargs)
		return __
	return _

class OLEUnitTest(unittest.TestCase):
	def setUpClass():
		collectSampleFiles(sampleFiles, globals())
	
	def test_parseSample(self):
		dirTarget = parseSample(sampleDirectory)
		self.assertEqual("52006F006F007400", bytesHex(dirTarget[:8]))
		self.assertEqual("72007900", bytesHex(dirTarget[16:20]))
	
	def test_emptySample(self):
		d = emptySample(0x200)
		self.assertEqual("00000000", bytesHex(d[:4]))
		
		d = emptySample(3)
		self.assertEqual("000000", bytesHex(d[:4]))
		
	def test_directoryEntry_root(self):
		dirTarget = parseSample(sampleDirectory)
		x = DirectoryEntry(dirTarget, 0)
		
		self.assertEqual("Root Entry", x.name)
		self.assertEqual(22, endint(x._dirEntry["_cb"]))
		self.assertEqual("RootStorage", x.type)
		self.assertEqual("Red", x.color)
		
		self.assertEqual("FFFFFFFF", bytesHex(x._dirEntry["_sidLeftSib"]))
		self.assertEqual("FFFFFFFF", bytesHex(x._dirEntry["_sidRightSib"]))
		self.assertEqual(1, endint(x._dirEntry["_sidChild"]))
		
		self.assertEqual(3, x.sectorStart)
		self.assertEqual(3456, x.sectorSize)
		self.assertEqual("SSAT", x.sectorType)
	
	def test_directoryEntry_user(self):
		dirTarget = parseSample(sampleDirectory2)
		x = DirectoryEntry(dirTarget, 0)
		
		self.assertEqual("Workbook", x.name)
		self.assertEqual(18, endint(x._dirEntry["_cb"]))
		self.assertEqual("UserStream", x.type)
		self.assertEqual("Red", x.color)
		
		self.assertEqual(2, endint(x._dirEntry["_sidLeftSib"]))
		self.assertEqual(4, endint(x._dirEntry["_sidRightSib"]))
		self.assertEqual("FFFFFFFF", bytesHex(x._dirEntry["_sidChild"]))
		
		self.assertEqual(0, x.sectorStart)
		self.assertEqual(2897, x.sectorSize)
		self.assertEqual("SSAT", x.sectorType)
	
	@copyOLE("buildMAST", "buildSAT", "buildSSAT", "buildDirectoryEntry")
	def test_ole(self, OLE):
		oleHeader = parseSample(sampleOldHeader)
		oleTarget = io.BytesIO(bytes(oleHeader))
		oleData = OLE(oleTarget)
		
		self.assertEqual("D0CF11E0A1B11AE1", bytesHex(oleData.bData[:8]))
		self.assertEqual("3B00", bytesHex(oleData.header["_uMinorVersion"]))
		self.assertEqual(512, oleData.header["sector size"])
		self.assertEqual(64, oleData.header["short sector size"])
		self.assertEqual(1, oleData.header["sat sector length"])
		self.assertEqual(10, oleData.header["first directory"])
		self.assertEqual(4096, endint(oleData.header["_ulMiniSectorCutoff"]))
		self.assertEqual(2, oleData.header["short sector start"])
		self.assertEqual(1, oleData.header["short sector number"])
		self.assertEqual(OLE.ENDOFCHAIN, endint(oleData.header["_sectDifStart"]))
		self.assertEqual(0, endint(oleData.header["_casectDif"]))
	
	@copyOLE("buildSAT", "buildSSAT", "buildDirectoryEntry")
	def test_oleBuildMast(self, OLE):
		oleHeader = parseSample(sampleOldHeader)
		oleTarget = io.BytesIO(bytes(oleHeader))
		oleData = OLE(oleTarget)
		
		self.assertEqual("00000000", oleData.MAST[0])
	
	@copyOLE("buildSSAT", "buildDirectoryEntry")
	def test_oleBuildSAT(self, OLE):
		oleHeader = parseSample(sampleOldHeader)
		satHeader = parseSample(sampleSat)
		oleTarget = io.BytesIO(bytes(oleHeader+satHeader))
		oleData = OLE(oleTarget)

		self.assertEqual("fffffffd", oleData.SAT[0])
		self.assertEqual("00000004", oleData.SAT[3])
		
	@copyOLE("buildDirectoryEntry")
	def test_oleBuildSAT(self, OLE):
		oleHeader = parseSample(sampleOldHeader)
		satHeader = parseSample(sampleSat)
		emptyData = emptySample(0x200)
		ssatHeader = parseSample(sampleSsat)
		oleTarget = io.BytesIO(bytes(oleHeader+satHeader+emptyData+ssatHeader))
		oleData = OLE(oleTarget)

		self.assertEqual("fffffffd", oleData.SAT[0])
		self.assertEqual("00000004", oleData.SAT[3])
	
	def test_realHwpFile(self):
		baseDir = os.path.dirname(__file__)
		hwpPath = "../sample/this is test hwp file.hwp"
		with open(os.path.join(baseDir,hwpPath), "rb") as f:
			oleData = OLE(f)
		
		for i in oleData.keys():
			self.assertTrue(i in hwpDirectoryList)
		