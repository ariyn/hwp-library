import unittest

from random import randint
from math import ceil

from src.hex import dump

def generateTestHexData(size=32):
	string = 0
	for i in range(0, ceil(size/4)):
		string = string << 16
		string += randint(0,255)
	return hex(string)

class HexUnitTest(unittest.TestCase):
	def test_testHexDataGenerator(self):
		hexData = generateTestHexData()
		print(hexData)
	
	def test_dump(self):
		hexData = generateTestHexData()
		print(hexData)
		for i in range(0, 32, 2):
			print(hexData[i:i+2], "%02x"%hexData[i:i+2])
		dump(hexData)
		
	def test_dump2(self):
		hexData = hex((255<<24)+(255<<16)+(255<8)+253)
		dump(hexData)