import unittest
import os

from src.hwp import HWP
from src.ole import OLE
import src.HWPID as HWPID
	

class HwpUnitTest(unittest.TestCase):
	def test_parseRealHWP(self):
		baseDir = os.path.dirname(__file__)
		hwpPath = "../sample/this is test hwp file.hwp"
			
		with open(os.path.join(baseDir,hwpPath), "rb") as f:
			hwpData = HWP(f)
	
		self.assertEqual(1, hwpData.isCompressed)
		self.assertEqual("5.0.5.0", hwpData.version)
		
		bodyText = hwpData.body.children[0].text
		self.assertEqual("this is test hwp file.", bodyText[16:-1])